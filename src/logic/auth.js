import axios from 'axios'

const END_POINT = 'https://ecommerce-p32-g4-backend.herokuapp.com/';

// const END_POINT = 'http://localhost:8000/'

const login = async (username, password) => {
    let response = axios.post(END_POINT + 'login/', {
        username: username,
        password: password
    })

    return response
}

const register = async (email, name, username, password, address) => {
    let response = axios.post(END_POINT + 'user/', {
        username,
        password,
        name,
        email,
        address,
        "account": {
            "is_active": true
        }
    })

    return response

}

export default { login, register }