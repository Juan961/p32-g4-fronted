import axios from 'axios'

const END_POINT = 'https://ecommerce-p32-g4-backend.herokuapp.com/';

// const END_POINT = 'http://localhost:8000/'

const getAll = async (name) => {
    let response;
    if(name){
        response = axios.get(END_POINT + `product/?name=${name}/`)

    }
    else {
        response = axios.get(END_POINT + 'product/')

    }

    return response
}

const getOne = async (id) => {
    let response = axios.get(END_POINT + 'product/' + id + "/")

    return response
}

const getByCategory = async (category) => {
    let response = axios.get(END_POINT + 'category/' + category + "/")

    return response
}

const createOne = async (name, price, photo, characteristics, category) => {
    let response = axios.post(END_POINT + 'product/', {
        name,
        price,
        photo,
        characteristics,
        category

    })

    return response
}

export default { getAll, getOne, getByCategory, createOne }