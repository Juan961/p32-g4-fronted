import { createRouter, createWebHistory } from 'vue-router'

import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import About from '../views/About.vue'

import HomeUser from '../views/HomeUser.vue'
import Categorias from '../views/Categorias.vue'
import Product from '../views/Product.vue'
import Busqueda from '../views/Busqueda.vue'
import Carrito from '../views/Carrito.vue'
import NewProduct from '../views/NewProduct.vue'

import Error from '../views/Error.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { title: 'Home' }
  },
  {
    path: '/about',
    name: 'About',
    component: About,
    meta: { title: 'About' }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { title: 'Login' }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: { title: 'Register' }
  },
  {
    path: '/home',
    name: 'HomeUser',
    component: HomeUser,
  },
  {
    path: '/categories',
    name: 'Categories',
    component: Categorias,
  },
  {
    path: '/search',
    name: 'Search',
    component: Busqueda,
  },
  {
    path: '/product/:id',
    name: 'Product',
    component: Product,
  }, 
  {
    path: '/cart',
    name: 'Cart',
    component: Carrito,
  },
  {
    path: '/new',
    name: 'New',
    component: NewProduct,
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'Error',
    component: Error
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router
